class Post < ActiveRecord::Base

  # Associations
  has_many :comments, dependent: :destroy

  # Validations
  validates_presence_of :title
  validates_presence_of :body

end
